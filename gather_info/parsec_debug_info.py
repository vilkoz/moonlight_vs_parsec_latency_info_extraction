# run from command `gdb --command parsec_debug_info.py /usr/bin/parsecd`
# works guaranteed only for parsec linux build 150-19

gdb.execute("b* main+0x26e")
gdb.execute("run")
i_sh_out = gdb.execute("i sh", to_string=True)
print("i_sh_out:", i_sh_out)
lines = []
for line in i_sh_out.split('\n'):
    if 'parsecd-150' in line:
        lines = line
print("lines:", lines)
addr = [x for x in lines.split(' ') if x != '']
print("addr:", addr)
b_addr = gdb.execute("find /b {}, {}, 0xe8, 0xa2, 0xc3, 0x36, 0x00".format(addr[0], addr[1]), to_string=True)

b_addr = b_addr.split('\n')[0].strip()
print("b_addr:", b_addr)
gdb.execute("b* {}".format(b_addr))
gdb.execute("c")
while True:
    encode_ms = gdb.execute("p *(float*)($r14)", to_string=True)
    decode_ms = gdb.execute("p *(float*)($r14+0x4)", to_string=True)
    network_ms = gdb.execute("p *(float*)($r14+0x8)", to_string=True)
    encode_ms, decode_ms, network_ms = [x.split('=')[1].strip() for x in (encode_ms, decode_ms, network_ms)]
    with open("/tmp/parsec_logs", "a") as f:
        f.write("""
Average receive time: {}
Average decoding time: {}
Average encoding time: {}
        """.format(network_ms, decode_ms, encode_ms))
    gdb.execute("c")

