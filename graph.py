import os
from select import poll, POLLIN
from time import sleep
from drawer import Drawer
from log_type import LOG_TYPE

def process_data(data, info):
    lines = data.split('\n')
    columns = [x.split(':') for x in lines]

    for column in columns:
        if LOG_TYPE == "moonlight":
            if 'Average receive time' in column[0]:
                info['receive'].append(column[1].strip().split()[0])
            elif 'Average decoding time' in column[0]:
                info['decode'].append(column[1].strip().split()[0])
            elif 'Average frame queue delay' in column[0]:
                info['queue'].append(column[1].strip().split()[0])
            elif 'Average rendering time' in column[0]:
                info['render'].append(column[1].strip().split()[0])
        else:
            if 'Average receive time' in column[0]:
                info['receive'].append(column[1].strip().split()[0])
            elif 'Average decoding time' in column[0]:
                info['decode'].append(column[1].strip().split()[0])
            elif 'Average encoding' in column[0]:
                info['encode'].append(column[1].strip().split()[0])
    return info


def main():
    if LOG_TYPE == "moonlight":
        f = open('/tmp/moonlight_logs', 'r')
    else:
        f = open('/tmp/parsec_logs', 'r')
    fd = f.fileno()

    p = poll()
    p.register(fd, POLLIN)

    if LOG_TYPE == "moonlight":
        info = {"receive": [],
                "decode": [],
                "queue": [],
                "render": [],
                }
    else:
        info = {"receive": [],
                "decode": [],
                "encode": [],
                }
    drawer = Drawer(info.keys())
    while not drawer.done:
        events = p.poll()
        print('[', end=' ')
        for e in events:
            print(e, end=' ')
        print(']')
        data = f.read()
        print('file: ', data)
        print('data: ', process_data(data, info))
        drawer.event_loop(info)

        sleep(1)




    f.close()

if __name__ == "__main__":
    main()
