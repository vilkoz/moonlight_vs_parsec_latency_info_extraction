import pygame
from pygame.locals import *
import os
from log_type import LOG_TYPE

class Drawer:
    def __init__(self, graph_keys):
        pygame.init()
        self.done = False
        self.timer = pygame.time.Clock()
        self.w = 500
        self.h = 100
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (1920 - self.w, 1080 - self.h)
        self.window = pygame.display.set_mode((self.w, self.h))
        self.graph_keys = graph_keys
        pygame.display.set_caption("Lattency graph")
        self.grid_color = (100,100,100)
        self.graph_color_dict = {"sum": (255, 0, 0)}
        self.graph_default_color = (255,255,255)

    def _get_graph_color_by_key(self, key):
        return self.graph_color_dict.get(key, self.graph_default_color)

    def _interpolate_points(self, points):
        out_points = []
        start = max(len(points) - 500, 0)
        stop = len(points)
        _points = points[start:stop]
        for x, point in enumerate(_points):
            point = min(self.h, point) # set max to self.h (100)
            point = self.h - point # 0 to bottom of the window 100 - to the top
            out_points.append((x, point))
        return out_points

    def _thin_out_points(self, points):
        out_points = []
        avg = 0
        count = 0
        for p in points:
            avg += p
            count += 1
            if count == 60:
                out_points.append(avg/count)
                avg = 0
                count = 0
        if count != 0:
            out_points.append(avg/count)
        return out_points

    def _draw_graph(self, graph_info):
        _graph_info = {key: [float(y) for y in graph_info[key]] for key in graph_info.keys()}
        if LOG_TYPE == "parsec":
            _graph_info = {key: self._thin_out_points(_graph_info[key]) for key in _graph_info.keys()}
        data = [_graph_info[key] for key in self.graph_keys]
        _graph_info["sum"] = [sum([float(y) for y in x]) for x in zip(*data)]
        print("_graph_info:", _graph_info)
        # clear window
        self.window.fill((0,0,0))
        # draw grid
        for n in range(0,self.h,10):
            start_pos = (0, n)
            end_pos = (self.w, n)
            pygame.draw.line(self.window, self.grid_color, start_pos, end_pos)
        for n in range(0, self.w, 50):
            start_pos = (n, 0)
            end_pos = (n, self.h)
            pygame.draw.line(self.window, self.grid_color, start_pos, end_pos)
        # draw graph
        for key in _graph_info.keys():
            points = self._interpolate_points(_graph_info[key])
            pygame.draw.lines(self.window, self._get_graph_color_by_key(key), False, points)

    def event_loop(self, graph_info):
        for event in pygame.event.get():
            if event.type == QUIT:
                self.done = True
        self._draw_graph(graph_info)

        pygame.display.update()
