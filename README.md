# moonlight_vs_parsec_latency_info_extraction

Code that I used to extract latencies from moonlight and parsec, and to draw them in realtime


## Setup environment for graph drawer

```
git clone URL_OF_THIS_REPOSITORY
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

Change the name of service latencies of which you want to draw in file `log_type.py`

```
$> cat log_type.py
# LOG_TYPE="moonlight"
LOG_TYPE="parsec"
```

Run the `graph.py` only after monitored service is launched, e.g:

```
# the command for the Parsec described below:
$> gdb --command gather_info/parsec_debug_info.py /usr/bin/parsecd
# in another terminal:
(venv) $> python3 graph.py
```

## Gathering info

### Moonlight-qt

1. Download moonlight-qt source

```
$> git clone https://github.com/moonlight-stream/moonlight-qt.git && cd moonlight-qt
$> git submodule update --init --recursive
```

2. Install build requirements and qt (more info at https://github.com/moonlight-stream/moonlight-qt)

3. (Optional) Checkout to tested commit
```
# I made patch for the following base commit:
# ee5c61f (origin/master, origin/HEAD, master) Centralize Discord invite links
$> git checkout ee5c61f
```

4. Apply patch for info output
```
$> git am FOLDER_OF_THIS_REPOSITORY/gather_info/0001-output-latencies-in-overlaymanager-into-file.patch
# If this command doesn't work try step #3
```

5. Build `moonlight-qt`

```
$> qmake moonlight-qt.pro
$> make debug
```

6. Run `moonlight-qt`

```
$> ./app/moonlight
```

### Parsec

Parsec latency info is gathered in hacky way by reverse engineering the parsec
binary and extracting latency values with debuger, the debuger causes the situation
when the video signal is 2-3 seconds behind to the real image (so it is not
failure of parsec or your network connection). So you should play on the host
computer while recording latency info.

!!! Only parsec build 150-19 for linux is tested to work

1. Use the following command to run parsec in debuger with the script provided

```
$> gdb --command gather_info/parsec_debug_info.py /usr/bin/parsecd
```

2. Connect to the host computer in parsec

3. Press 'c' in debuger when parsec freezes
